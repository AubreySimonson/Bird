﻿/**Part of Bird User Test 2, a study intended to be conducted in the Spring of 2020, 
 * during the pandemic, to make the VRST deadline in June 
 * 
 *  It belongs on a gameobject representing the cursor, which is a child of the hand.
 *  It is temporary, and should be replaced for Bird implemented with hand tracking as soon as I have access to an Oculus Quest

   Please contact asimonso@mit.edu for more information

   Last modified June, 2020
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Valve.VR;
using Oculus.Avatar;//you're also not quite sure if this is the right namespace
namespace cakeslice//for highlighting
{

    public class Reel : MonoBehaviour
    {
        //public AudioSource clickOn;
        //public AudioSource clickOff;

        //public SteamVR_ActionSet actionSetEnable;
        //public SteamVR_Action_Boolean hold;
        //public SteamVR_Action_Boolean extend;
        //public SteamVR_Action_Boolean retract;
        private GameObject touchedObject;
        public GameObject targetManager;
        private bool holding;
        private float extensionDistance;
        public float extendSpeed;

        public Material red;

        public TestManager testManager;

        private bool rightTriggerIsDown = false;//a variable which only has to exist because Oculus doesn't have "GetTriggerDown" or "GetTriggerUp"

        private void Start()
        {
            touchedObject = null;
            holding = false;
            extensionDistance = 0.0f;
        }

        // Update is called once per frame
        void Update()
        {
            //button input
            //if (hold.GetStateDown(SteamVR_Input_Sources.Any))
            //if()//switch to "trigger down"
            //{
            //    touchedObject.transform.parent = gameObject.transform;
            //    if (touchedObject.tag == "red")
            //    {
            //        touchedObject.AddComponent<RedSphere>();//a script for telling test manager if it hits something
            //    }
            //    holding = true;
                //clickOn.Play();
            //}
            //if (hold.GetStateUp(SteamVR_Input_Sources.Any))
            //if()//trigger up
            //{
            //    touchedObject.transform.parent = targetManager.transform;
            //    holding = false;
                //clickOff.Play();
            //}
            //if (extend.GetState(SteamVR_Input_Sources.Any))
            if (OVRInput.Get(OVRInput.Button.Two) || OVRInput.Get(OVRInput.Button.Four))
            {
                extensionDistance += extendSpeed;
                transform.localPosition = new Vector3(0.0f, 0.0f, extensionDistance);
            }
            //if (retract.GetState(SteamVR_Input_Sources.Any))
            if (OVRInput.Get(OVRInput.Button.One) || OVRInput.Get(OVRInput.Button.Three))
            {
                extensionDistance -= extendSpeed;
                transform.localPosition = new Vector3(0.0f, 0.0f, extensionDistance);
            }
            //remove objects stuck to the hand, if they don't un-stick when they're supposed to
            if (!holding && gameObject.transform.childCount > 0)
            {
                //will only un-stick one object per frame
                gameObject.transform.GetChild(0).transform.parent = targetManager.transform;
            }

            //code related to holding which only needs to exist because Oculus doesn't have "GetTriggerDown" or "GetTriggerUp"
            if (!rightTriggerIsDown)
            {
                if ((OVRInput.Get(OVRInput.Axis1D.SecondaryIndexTrigger))>0.5f)//if right trigger is down--this might be backwards
                {
                    rightTriggerIsDown = true;
                    GetTriggerDown();
                }
            }
            if (rightTriggerIsDown)
            {
                if ((OVRInput.Get(OVRInput.Axis1D.SecondaryIndexTrigger)) < 0.5)//if right trigger is up--this might be backwards
                {
                    rightTriggerIsDown = false;
                    GetTriggerUp();
                }
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "grabbable" || other.gameObject.tag == "red")
            {
                //other.gameObject.AddComponent<Outline>();//the problem is currently with outline -- maybe come back to this one?
                //other.gameObject.GetComponent<Outline>().color = 2;
                touchedObject = other.gameObject;
            }
        }

        //the following two functions exist exclusively because Oculus doesn't have a "Get Trigger Down" function anywhere
        public void GetTriggerDown()
        {
            touchedObject.transform.parent = gameObject.transform;
                if (touchedObject.tag == "red")
                {
                    touchedObject.AddComponent<RedSphere>();//a script for telling test manager if it hits something
                }
                holding = true;
            //clickOn.Play();
        }
        public void GetTriggerUp()
        {
                touchedObject.transform.parent = targetManager.transform;
                holding = false;
            //clickOff.Play();
        }
        //uncomment to disable sticky select-- we just have it on because it is so hard to use this thing (reel)
        //private void OnTriggerExit(Collider other)
        //{
        //    if (other.gameObject.tag == "grabbable")
        //    {
        //        Destroy(other.gameObject.GetComponent<Outline>());
        //        if (!holding)
        //        {
        //            touchedObject = null;
        //        }
        //    }
        //}
    }
}
