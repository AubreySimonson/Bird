﻿/*This script is part of the user testing for bird.  It controls one of the many control methods tested-- 
 * our actual experimental method-- the bird.   
 * It belongs on a gameobject representing the cursor, which is a child of the hand.
 * It's a little fussier than the other methods, because it needs to talk to an arduino.
 * Remember that things with api compatability are super weird.  This might help:
 * https://answers.unity.com/questions/1604233/systemioports-missing-for-unity-with-net-4x.html
 * 
 * Reminder to make a user ball their fist up really small and open it really wide a few times to callibrate.
 * 
   Please contact asimonso@mit.edu for more information

   August, 2019
 */
//using System.IO.Ports;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace cakeslice//for highlighting
{

    public class Bird : MonoBehaviour
    {
        private GameObject touchedObject;
        public GameObject targetManager;
        private bool holding;

        public AudioSource clickOn;
        public AudioSource clickOff;

        public float clickThreshold;
        private float pointerValue;
        private bool clicking;

        private float extensionDistance;//distance between cursor and hand
        public float openness;//read from arduino
        public float maxDistance;//how far from the hand do you want the cursor to be able to extend?
        private float minOpenness;//will be the higher value, defaults to 0
        private float maxOpenness = 9000.0f;//will be the lower value, start it unrealistically high
        private float scaleFactor;

        //SerialPort stream = new SerialPort("COM3", 115200, Parity.None, 8, StopBits.One); // Check bottom right corner of Arduino IDE for COM #
        static int[] bendValues = new int[5]; // Static might be dumb if you want more than one hand
        private void Start()
        {
            //stream.Open();
            //stream.ReadTimeout = 50;
            touchedObject = null;
            holding = false;
            extensionDistance = 0.0f;
        }// end start

        // Update is called once per frame
        void Update()
        {
            Debug.Log("um, hello!?");
            //remove objects stuck to the hand, if they don't un-stick when they're supposed to
            if (!holding && gameObject.transform.childCount > 0)
            {
                //will only un-stick one object per frame
                gameObject.transform.GetChild(0).transform.parent = targetManager.transform;
            }

            //read from arduino
            openness = 0;
            //stream.ReadExisting();
            //stream.ReadTo("start");
            //string deviceMsg = stream.ReadLine();
            //string[] msgBlocks = deviceMsg.Split(';');
           //string[] bendValStrs = msgBlocks[0].Split(' ');
            for (int i = 0; i < 5; i++)
            {
                //bendValues[i] = int.Parse(bendValStrs[i]);
            }
            openness = bendValues[0] + bendValues[2] + bendValues[3] + bendValues[4];//exclude pointer

            //update scaling algorithm
            //lower values mean more open
            if(openness >= minOpenness)
            {
                minOpenness = openness;
                scaleFactor = maxDistance / (minOpenness - maxOpenness);
            }
            if(openness <= maxOpenness && openness > 10.0f)//don't count dropped frames
            {
                maxOpenness = openness;
                scaleFactor = maxDistance / (minOpenness - maxOpenness);
            }
            extensionDistance = openness * scaleFactor;

            //update cursor position
            extensionDistance = maxDistance - extensionDistance;
            extensionDistance += 0.1f;//it tends to inexplicably overshoot?  Adjust until comfortable.
            transform.localPosition = new Vector3(0.0f, 0.0f, extensionDistance);

            //clicking 
            Debug.Log(bendValues[1]);
            if (bendValues[1] >= clickThreshold)
            {
                if (!clicking && !holding)//select
                {
                    touchedObject.transform.parent = gameObject.transform;
                    clicking = true;
                    clickOn.Play();
                    holding = true;
                }
                if(!clicking && holding)//deselect
                {
                    clicking = true;
                    clickOff.Play();
                    touchedObject.transform.parent = targetManager.transform;
                    holding = false;
                }
            }
            else
            {
                clicking = false;
            }
        }//end update

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "grabbable")
            {
                if (touchedObject != null)
                {
                    Destroy(touchedObject.gameObject.GetComponent<Outline>());
                }
                other.gameObject.AddComponent<Outline>();
                touchedObject = other.gameObject;
            }
        }
        //uncomment to get rid of stickytouch
        //private void OnTriggerExit(Collider other)
        //{
        //    if (other.gameObject.tag == "grabbable")
        //    {
        //        Destroy(other.gameObject.GetComponent<Outline>());
        //        if (!holding)
        //        {
        //            touchedObject = null;
        //        }
        //    }
        //}//end ontriggerexit
        //communicating with arduino
        void OnApplicationQuit()
        {

            //stream.Close();

        }
    }
}
