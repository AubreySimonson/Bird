﻿/**Part of Bird User Test 2, a study intended to be conducted in the Spring of 2020, during the pandemic, to make the VRST deadline in July
 * experimentmanager.cs controlls starting different trials of the test, and all of the UI
 * 
 * 
 * ???--->asimonso@mit.edu
 * last modified July 2020
 * 
 **/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Android;
using UnityEngine.UI;

public class ExperimentManager : MonoBehaviour
{
    public int version;//there should be 4 versions of the test, for counterbalancing reasons (check back in with that class about this.  anyways, it's easy to change)
    private int[] trialOrder;
    private int[] v1;
    private int[] v2;
    private int[] v3;
    private int[] v4;
    private int trialCounter;

    public consoleUI console;

    private float timer;//how many seconds has the scene gone on for
    public float trialLength;//remember that time is in seconds
    private bool trialIsHappening;//so that we know when to count

    public GameObject introText, calibrationUI, startTrialUI, endTrialUI, exitUI, UICanvas;

    public TestManager testManager;

    public QuestBird bird;//remember to plug this in

    //for slowing down text so that people read it
    float delayTime = 0.0f;
    float delayTimeTimer = 0.0f;
    GameObject delayed;


    // Start is called before the first frame update
    void Start()
    {
        if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
            Permission.RequestUserPermission(Permission.ExternalStorageWrite);
        testManager.LogArbitrary("Experiment start.  Version = " + version + "***");
        console.log("writer worked!");

        v1 = new int[] { 0, 1, 3, 2 };
        v2 = new int[] { 1, 2, 0, 3 };
        v3 = new int[] { 2, 3, 1, 0 };
        v4 = new int[] { 3, 0, 2, 1 };
        if(version == 1)
        {
            trialOrder = v1;
        }
        else if(version == 2)
        {
            trialOrder = v2;
        }
        else if (version == 3)
        {
            trialOrder = v3;
        }
        else if(version == 4)
        {
            trialOrder = v4;
        }
        else
        {
            console.log("Invalid version specified");
        }
        trialCounter = 0;//++ at the end of each trial.

        //instantiate intro panel
        UICanvas.SetActive(true);
        introText.SetActive(true);
        GameObject introButton = introText.transform.Find("Button").gameObject;
        Delay(introButton, 15.0f);//the button shouldn't show up for 15 seconds
        startTrialUI.SetActive(false);
        endTrialUI.SetActive(false);
        exitUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (trialIsHappening)
        {
            //do timer stuff
            if (timer <= trialLength)
            {
                timer += Time.deltaTime;
            }
            else
            {
                endTrial();
            }
        }
        if(delayed != null)
        {
            if(delayTimeTimer >= delayTime)
            {
                delayed.SetActive(true);
                delayed = null;
            }
            else
            {
                delayTimeTimer += Time.deltaTime;
            }
        }
    }

    //called by introUI, calls NextScene
    public void Calibration()
    {
        introText.SetActive(false);
        calibrationUI.SetActive(true);
        GameObject callibrationButton = calibrationUI.transform.Find("Button").gameObject;
        Delay(callibrationButton, 15.0f);//the button shouldn't show up for 15 seconds
        bird.calibration = true;
    }

    public void NextScene()
    {
        if(bird.calibration == true)
        {
            if (bird.CheckCallibration())//if we're actually callibrated
            {
                bird.calibration = false;
                testManager.LogArbitrary("CALIBRATION DATA: ***");
                bird.LogCalibrationData();
            }
            else
            {
                calibrationUI.transform.Find("Error").gameObject.SetActive(true);
            }
        }

        //just in case these are on...
        calibrationUI.SetActive(false);
        endTrialUI.SetActive(false);

        //log that a new trial has started
        testManager.LogArbitrary("Tutorial start.  Trial # = " + trialCounter + ", condition # = " + trialOrder[trialCounter] + " at " + Time.time + "***");

        //Instantiate startTrialPrefab
        introText.SetActive(false);
        startTrialUI.SetActive(true);
        //Text startTrialText = startTrialUI.GetComponentInChildren<Text>();

        //set text of startTrialPrefab for the right trial
        if(trialOrder[trialCounter] == 0)//pointer click, no penumbra
        {
            //startTrialText.text = 
            //    "The distance of your fingers from the red dot controls the distance of the bird from your hand. " +
            //    "Use your pointer finger to click.  The bird will only select a sphere if it is currently in that sphere. " +
            //    "Take a minute to practice.Grab the red sphere, and move it to the blue sphere.  When you feel ready, " +
            //    "click next, and repeat this task as many times as you can in 5 minutes.";
            startTrialUI.transform.Find("Start1").gameObject.SetActive(true);
            //turn the other 3 off
            startTrialUI.transform.Find("Start2").gameObject.SetActive(false);
            startTrialUI.transform.Find("Start3").gameObject.SetActive(false);
            startTrialUI.transform.Find("Start4").gameObject.SetActive(false);
            //tell bird which finger clicks, if it should do radius things
            console.log("inTarget, index");
            bird.SetSelectionType("inTarget");
            bird.SwitchClickingFinger("index");
        }
        else if(trialOrder[trialCounter] == 1)//pointer click, with penumbra
        {
            //startTrialText.text =
            //    "The distance of your fingers from the red dot controls the distance of the bird from your hand. " +
            //    "Use your pointer finger to click.  The bird will select whichever sphere it is closest to. " +
            //    "Take a minute to practice.Grab the red sphere, and move it to the blue sphere.  When you feel ready, " +
            //    "click next, and repeat this task as many times as you can in 5 minutes.";
            startTrialUI.transform.Find("Start2").gameObject.SetActive(true);
            //turn the other 3 off
            startTrialUI.transform.Find("Start1").gameObject.SetActive(false);
            startTrialUI.transform.Find("Start3").gameObject.SetActive(false);
            startTrialUI.transform.Find("Start4").gameObject.SetActive(false);
            //tell bird which finger clicks, if it should do radius things
            console.log("nearest, index");
            bird.SetSelectionType("nearest");
            bird.SwitchClickingFinger("index");
        }
        else if(trialOrder[trialCounter] == 2)//thumb click, no penumbra
        {
            //startTrialText.text =
            //    "The distance of your fingers from the red dot controls the distance of the bird from your hand. " +
            //    "Use your thumb to click.  The bird will only select a sphere if it is currently in that sphere. " +
            //    "Take a minute to practice.Grab the red sphere, and move it to the blue sphere.  When you feel ready, " +
            //    "click next, and repeat this task as many times as you can in 5 minutes.";
            startTrialUI.transform.Find("Start3").gameObject.SetActive(true);
            //turn the other 3 off
            startTrialUI.transform.Find("Start1").gameObject.SetActive(false);
            startTrialUI.transform.Find("Start2").gameObject.SetActive(false);
            startTrialUI.transform.Find("Start4").gameObject.SetActive(false);
            //tell bird which finger clicks, if it should do radius things
            console.log("inTarget, thumb");
            bird.SetSelectionType("inTarget");
            bird.SwitchClickingFinger("thumb");
        }
        else if(trialOrder[trialCounter] == 3)//thumb click, with penumbra
        {
            //startTrialText.text =
            //    "The distance of your fingers from the red dot controls the distance of the bird from your hand. " +
            //    "Use your thumb to click.  The bird will select whichever sphere it is closest to. " +
            //    "Take a minute to practice.Grab the red sphere, and move it to the blue sphere.  When you feel ready, " +
            //    "click next, and repeat this task as many times as you can in 5 minutes.";
            startTrialUI.transform.Find("Start4").gameObject.SetActive(true);
            //turn the other 3 off
            startTrialUI.transform.Find("Start1").gameObject.SetActive(false);
            startTrialUI.transform.Find("Start2").gameObject.SetActive(false);
            startTrialUI.transform.Find("Start3").gameObject.SetActive(false);
            //tell bird which finger clicks, if it should do radius things
            console.log("nearest, thumb");
            bird.SetSelectionType("nearest");
            bird.SwitchClickingFinger("thumb");
        }
        else
        {
            console.log("Invalid trial specified");
        }

        //tell testmanager to instantiate spheres
        //testManager.CreateSpheres();//----------------->StartTestManager does this now
    }

    //gets called by pushing the "start" button on any tutorialUI
    public void startTrial()
    {

        //log trial start 
        testManager.LogArbitrary("Trial start.  Trial # = " + trialCounter + ", condition # = " + trialOrder[trialCounter] +", time = " + Time.time + "***");

        //get rid of trial text
        startTrialUI.SetActive(false);
        trialIsHappening = true;
        timer = 0;
    }


    //gets called when timer >= sceneLength
    public void endTrial()
    {
        trialIsHappening = false;
        //log trial end
        testManager.LogArbitrary("Trial end.  Trial # = " + trialCounter + ", condition # = " + trialOrder[trialCounter] + ", time = " + Time.time + "***");

        //get rid of al of the spheres (bakers, step away from the cakes!)
        testManager.RemoveSpheres();

        //check if that was the last trial.  If it was, instantiate the special end text.
        if (trialCounter > 3)//there might be an off by 1 error here
        {
            exitUI.SetActive(true);
        }
        else
        {
            //instantiate end of trial text
            endTrialUI.SetActive(true);
            GameObject endTrialButton = endTrialUI.transform.Find("Button").gameObject;
            Delay(endTrialButton, 30.0f);//the button shouldn't show up for 30 seconds
        }
        trialCounter++;
    }

    //for slowing down text so that people read it
    public void Delay(GameObject delayThis, float forThisLong)
    {
        delayed = delayThis;
        delayed.SetActive(false);
        delayTimeTimer = 0.0f;
        delayTime = forThisLong;
    }
}
