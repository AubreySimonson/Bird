﻿/**Part of Bird User Test 2, a study intended to be conducted in the Spring of 2020, during the pandemic, to make the VRST deadline in July
 * BirdClick.cs creates a system for clicking on buttons with the Bird.  Attach this and a trigger collider to the button, 
 * or an object in the same location.
 * 
 * Remember to plug in the gameobject you're trying to talk to
 * 
 * ???--->asimonso@mit.edu
 * last modified June 2020
 * 
 **/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdClick : MonoBehaviour
{
    public GameObject hasScript;//we want to talk to this one
    public string function;//the name of the function we want to call.  Remember to not use ()

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Bird")//this is a pretty fragile way to code this
        {
            hasScript.BroadcastMessage(function);
        }
    }
}
