﻿/**Part of Bird User Test 2, a study intended to be conducted in the Summer of 2020, during the pandemic, to make the IEEEVR Deadline in September
 * TestManager.cs is the script that runs individual trials.  It:
 *   generates a field of spheres which can be grabbed by the bird
 *   turns one of these spheres red and another blue
 *   when the red sphere has been docked in the blue sphere, it puts them both back and starts over
 *   logs data such as
 *      [xx]the settings callibrated by the callibration phase
 *      [xx]locations of all spheres each trial
 *      [xx]time and location of each red sphere and blue sphere when designated as the red and blue spheres
 *      [xx]HANDLED BY QUEST BIRD -- the time, location (of both bird and clickee), and type of every object selected by a click
 *      [xx]HANDLED BY QUEST BIRD -- the time and location of the bird every time a click happens which selects nothing
 *      [xx]HANDLED BY EXPERIMENT MANAGER -- The time spent in each section (training, actual trial, rest/survey)
 *      [xx]The time and location every time the blue sphere touches the red sphere
 *      
 * 
 * ???--->asimonso@mit.edu
 * last modified July 2020
 * 
 **/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Android;


public class TestManager : MonoBehaviour
{
    public GameObject startGameText;// a text which explains the test to the user
    public GameObject endGameText;// a text which tells the user that the trial is over, and to go back to the survey

    private int totalSpheres = 50;
    private float minY, maxY, minX, maxX, minZ, maxZ;
    private float testLength = 300;//test length (in seconds)

    private string path;//path to save file
    private StreamWriter writer;

    private List<GameObject> spheres;
    private GameObject redSphere;
    public Material red;
    public Material blue;
    public Material defaultmat;
    private GameObject blueSphere;
    private GameObject bird;
    public QuestBird questBird;//for getting at deselect
    private Vector3 redSphereOriginalPosition;

    //this is for if someone holds onto something for too long
    private float tooLong;
    private float tooLongTimer;
    private bool testActive = false;

    public consoleUI console;



    // Start is called before the first frame update
    void Start()
    {
        path = "/sdcard/Download/log.txt";
        tooLong = 30.0f;
        tooLongTimer = 0.0f;
    }

    private void Update()
    {
        if (testActive)
        {
            if (tooLongTimer >= tooLong)
            {
                writer = new StreamWriter(path, true);
                writer.WriteLine("Took too long.  Moving on to next set of spheres at " + Time.time);//this never writes
                writer.Close();
                SwapSpheres();

            }
            else
            {
                tooLongTimer += Time.deltaTime;
            }
        }
    }

    public void CreateSpheres()
    {
        testActive = true;
        writer = new StreamWriter(path, true);
        writer.WriteLine("Creating spheres at " + Time.time + "***");
        spheres = new List<GameObject>();
        minX = -8.0f;
        maxX = 8.0f;
        minY = 0.0f;
        maxY = 8.0f;
        minZ = -8.0f;
        maxZ = 8.0f;
        //create spheres and add them to spheres list 
        for (int i = 0; i <= totalSpheres; i++)
        {
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.transform.parent = gameObject.transform;
            sphere.transform.position = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), Random.Range(minZ, maxZ));
            sphere.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            sphere.AddComponent<SphereCollider>();
            sphere.AddComponent<Rigidbody>();
            sphere.GetComponent<Rigidbody>().isKinematic = true;
            sphere.GetComponent<Rigidbody>().useGravity = false;
            sphere.GetComponent<Renderer>().material = defaultmat;
            sphere.tag = "grabbable";
            spheres.Add(sphere);
            writer.WriteLine("Sphere location: " + sphere.transform.position + "***");
        }
        writer.Close();
        SwapSpheres();
    }

    public void RemoveSpheres()
    {
        testActive = false;
        foreach(GameObject sphere in spheres)
        {
            Destroy(sphere);
        }
    }

    private void SwapSpheres()
    {
        ReturnSpheres();
        questBird.Deselect();
        tooLongTimer = 0.0f;
        writer = new StreamWriter(path, true);
        writer.WriteLine("Swapping Spheres at " + Time.time + "***");
        //clean off the old spheres
        if (redSphere)//don't break on the first frame...
        {
            writer.WriteLine("resetting red sphere");
            redSphere.GetComponent<Renderer>().material = defaultmat;
            redSphereOriginalPosition = redSphere.transform.position;
            redSphere.tag = "grabbable";
            Destroy(redSphere.GetComponent<RedSphere>());
        }
        if (blueSphere)
        {
            writer.WriteLine("resetting blue sphere");
            blueSphere.GetComponent<Renderer>().material = defaultmat;
            blueSphere.tag = "grabbable";
        }

        int redNum = Random.Range(0, totalSpheres);
        int blueNum = Random.Range(0, totalSpheres);
        while(blueNum == redNum)
        {
            //keep redrawing the second sphere until it is not the same sphere again
            blueNum = Random.Range(0, totalSpheres);
        }
        redSphere = spheres[redNum];
        redSphere.GetComponent<Renderer>().material = red;
        redSphere.gameObject.tag = "red";
        redSphere.AddComponent<RedSphere>();
        writer.WriteLine("New red sphere position: " + redSphere.transform.position + "***");
        blueSphere = spheres[blueNum];
        blueSphere.GetComponent<Renderer>().material = blue;
        blueSphere.gameObject.tag = "blue";
        writer.WriteLine("New blue sphere position: " + blueSphere.transform.position + "***");
        writer.Close();
    }

    public void RedSphereHitSomething(Collider other)//<--------------------what calls this?
    {
        //if the red sphere enters something
        //was it the blue sphere or something else?
        if (other.gameObject.tag == "blue")
        {
            //log it
            writer = new StreamWriter(path, true);
            writer.WriteLine("Red sphere contacted blue sphere at TIME:" + Time.time + "and LOCATION: " + redSphere.transform.position + "***");//<-------------check the log for if this ever gets called
            writer.Close();

            //then put the red sphere back where it goes
            redSphere.transform.position = redSphereOriginalPosition;

            //and start over
            SwapSpheres();
        }
    }

    public GameObject FindNearestGrabbable(Transform target)
    {
        float smallestDistance = 9999f;
        GameObject closestSphere = null;
        foreach(GameObject sphere in spheres)
        {
            float dist = Vector3.Distance(sphere.transform.position, target.transform.position);
            if(dist < smallestDistance)
            {
                smallestDistance = dist;
                closestSphere = sphere;
            }
        }
        return closestSphere;
    }

    public void LogArbitrary(string data)
    {
        writer = new StreamWriter(path, true);
        writer.WriteLine(data);
        writer.Close();
    }

    //if any spheres have been accidentally moved outside of the room, return them
    private void ReturnSpheres()
    {
        foreach(GameObject sphere in spheres)
        {
            if(sphere.transform.position.x < minX || sphere.transform.position.x > maxX || sphere.transform.position.y < minY || sphere.transform.position.y > maxY || sphere.transform.position.z < minZ || sphere.transform.position.z > maxZ)
            {
                sphere.transform.position = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), Random.Range(minZ, maxZ));
            }
        }
    }
}
