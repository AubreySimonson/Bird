﻿/**Part of Bird User Test 2, a study intended to be conducted in the Summer of 2020, 
 * during the pandemic, to make the IEEEVR Deadline in September 
 * 
 *  Breaks up start trial text into 3 parts with delays between them so that people are
 *  more likely to actually read it

   Please contact asimonso@mit.edu for more information

   Last modified July, 2020
 */

 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartTextManager : MonoBehaviour
{
    private GameObject part1, part2, nextButton;
    private float timer;
    public TestManager testManager;
    private bool spheresCreated = false;

    // Start is called before the first frame update
    void Start()
    {
        //getting into the habit of doing all of this by name is kind of fragile and probably a bad thing?
        part1 = gameObject.transform.Find("part1").gameObject;
        part2 = gameObject.transform.Find("part2").gameObject;
        nextButton = gameObject.transform.Find("Button").gameObject;
        timer = 0.0f;
        part1.SetActive(true);
        part2.SetActive(false);
        nextButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (part1.activeSelf && timer >= 10.0f)
        {
            part1.SetActive(false);
            part2.SetActive(true);
        }
        else if(spheresCreated == false && part2.activeSelf && timer >= 20.0f)
        {
            testManager.CreateSpheres();
            spheresCreated = true;
        }
        else if(part2.activeSelf && timer >= 30.0f)
        {
            nextButton.SetActive(true);
        }
        timer += Time.deltaTime;
    }
}
