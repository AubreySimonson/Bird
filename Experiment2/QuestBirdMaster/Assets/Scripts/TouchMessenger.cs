﻿/**Part of Bird User Test 2, a study intended to be conducted in the Spring of 2020, during the pandemic, to make the VRST deadline in June29th
 *
 * A small helper script to put on an object, and make it report up to QuestBird.  
 * There are more modular, elegant ways to do this, but you're more interested in "works" than "elegant" right now
 * 
 * 
 * ???--->asimonso@mit.edu
 * last modified June 2020
 * 
 **/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchMessenger : MonoBehaviour
{
    public QuestBird reportTo;

    private void OnTriggerEnter(Collider other)
    {
        reportTo.informOfTouch(other.gameObject);
    }
}
