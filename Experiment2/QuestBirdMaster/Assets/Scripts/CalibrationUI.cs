﻿/**Part of Bird User Test 2, a study intended to be conducted in the Spring of 2020, during the pandemic, to make the IEEEVR deadline in September
 *
 * A small piece of UI to tell us about the max, min, and current extension of every finger.
 * Doesn't built the UI-- you have to do that yourself, and plug in all 15 text boxes.
 * 
 * 
 * ???--->asimonso@mit.edu
 * last modified July 2020
 * 
 **/
 
 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalibrationUI : MonoBehaviour
{
    public QuestBird bird;
    public Text thumbMax, thumbMin, thumbCurrent, indexMax, indexMin, indexCurrent, middleMax, middleMin, middleCurrent, ringMax, ringMin, ringCurrent, pinkyMax, pinkyMin, pinkyCurrent;

    // Update is called once per frame
    void Update()
    {
        //you really aren't feeling up to doing something more clever than just hardcoding all of this
        //you have to get at them via the arrays that they're stored in, rather than just saying, like, thumbMax.
        //you don't know why
        thumbMax.text = "Thumb Max: " + bird.maxExtensions[0];
        thumbCurrent.text = "Thumb Current: " + bird.currentExtensions[0];
        thumbMin.text = "Thumb Min: " + bird.minExtensions[0];
        indexMax.text = "Index Max: " + bird.maxExtensions[1];
        indexCurrent.text = "Index Current: " + bird.currentExtensions[1];
        indexMin.text = "Index Min: " + bird.minExtensions[1];
        middleMax.text = "Middle Max: " + bird.maxExtensions[2];
        middleCurrent.text = "Middle Current: " + bird.currentExtensions[2];
        middleMin.text = "Middle Min: " + bird.minExtensions[2];
        ringMax.text = "Ring Max: " + bird.maxExtensions[3];
        ringCurrent.text = "Ring Current: " + bird.currentExtensions[3];
        ringMin.text = "Ring Min: " + bird.minExtensions[3];
        pinkyMax.text = "Pinky Max: " + bird.maxExtensions[4];
        pinkyCurrent.text = "Pinky Current: " + bird.currentExtensions[4];
        pinkyMin.text = "Pinky Min: " + bird.maxExtensions[4];
    }
}
