﻿/**Part of Bird User Test 2, a study intended to be conducted in the Summer of 2020, 
 * during the pandemic, to make the IEEEVR Deadline in September 
 * 
 *  Funcitonality for a slider which changes the scale of all text involved in the experiment.
 *  Attach to the line part of the slider, which should have a rigidbody and trigger collider

   Please contact asimonso@mit.edu for more information

   Last modified July, 2020
 */
 
 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextScaler : MonoBehaviour
{
    //the parts of the slider.  There are probably real words for the parts?  But these are the shapes that they are.
    public GameObject line;
    public GameObject sphere;
    public GameObject textScaler;

    public GameObject hand;
    public GameObject[] thingsWithText;



    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    //there might be the potential for weird bugs here if you, like, touch the slider with both the hand and the bird
    //but it's a lot more code to be sure that that won't happen
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == hand)
        {
            sphere.transform.parent = hand.transform;
            sphere.transform.localScale = new Vector3(1, 1, 1);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == hand)
        {
            float tempX = sphere.transform.position.x;
            sphere.transform.parent = textScaler.transform;
            sphere.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
            sphere.transform.localScale = new Vector3(10, 20, 10);
            sphere.transform.position = new Vector3(tempX, 0.0f, 0.0f);
        }
    }
}
