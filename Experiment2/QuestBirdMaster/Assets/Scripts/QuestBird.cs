﻿/**Part of Bird User Test 2, a study intended to be conducted in the Spring of 2020, during the pandemic, to make the VRST deadline in June29th
 *
 * Bird, recoded for Quest hands.  Only for the right hand, for now, but you're keeping in mind making it easy to switch.
 * 
 * 
 * ???--->asimonso@mit.edu
 * last modified July 2020
 * 
 **/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Android;
using UnityEngine.UI;

public class QuestBird : MonoBehaviour
{
    public GameObject[] allFingers;//includes clicking finger
    public GameObject thumb, index, middle, ring, pinky;//these all need attached manually
    //public for debugging.  should probably eventually be made private
    public float thumbMax, thumbMin, thumbCurrent, indexMax, indexMin, indexCurrent, middleMax, middleMin, middleCurrent, ringMax, ringMin, ringCurrent, pinkyMax, pinkyMin, pinkyCurrent;
    //ditto
    public float[] currentExtensions, minExtensions, maxExtensions;
    public bool calibration;//turn on if calibrating
    private bool notCalibratedYet;
    public bool isLeft;

    public Transform centerPoint;//a point some distance in front of the palm.  You need to add this to the hand model manually.

    private float extensionDistance;//how far the bird should be from the hand this frame
    private float adjustedHandExtension;//how far fingers being used to control hand extension are from centerpoint this frame
    public float maxDistance, adjustedMax, setBack;
    private float thisFrame = 0.0f;
    private float lastFrame = 0.0f;//for slight smoothing

    public GameObject bird;

    public GameObject clickingFinger;
    public string selectionType;
    public AudioSource clickOn;
    public AudioSource clickOff;

    private GameObject touchedObject;//what object did we last touch?
    public GameObject targetManager;//to return objects to after we pick them up
    private bool holding;//are we currently holding something?
    private bool clicking;//are we currently clicking?
    private float clickRangeMin;
    private float clickRangeMax;

    public TestManager testManager;

    public consoleUI console;//for debugging
    public Text AreWeClicking;//a whole ass separate piece of UI just to figure out if we are currently clicking because you are currently frustrated by an extraordinary bug
    public Text ClickFingerDisplay;//the same, but to debug if we switched fingers correctly

    public Material defaultMat, defaultHilight, red, redHilight, blue, blueHilight;

    //a quick timer system that prevents a horrible mess of rapidly clicking and unclicking every time the Quest's hand tracking flips out
    //(which happens often.)
    private int antiBounceFrames;
    private int antiBounceFramesTimer;



    // Start is called before the first frame update
    void Start()
    {
        extensionDistance = 0.0f;
        maxDistance = 20.0f;
        adjustedMax = 0.0f;
        setBack = 5.0f;
        antiBounceFrames = 4;
        antiBounceFramesTimer = 0;

        touchedObject = null;
        holding = false;
        clicking = false;
        calibration = false;
        notCalibratedYet = true;
        AreWeClicking.text = "false";

        thumbMin = 9999.9f;
        indexMin = 9999.9f;
        middleMin = 9999.9f;
        ringMin = 9999.9f;
        pinkyMin = 9999.9f;
        minExtensions = new float[5] { thumbMin, indexMin, middleMin, ringMin, pinkyMin };

        thumbMax = 0.0f; 
        indexMax = 0.0f;
        middleMax = 0.0f;
        ringMax = 0.0f;
        pinkyMax = 0.0f;
        maxExtensions = new float[5] { thumbMax, indexMax, middleMax, ringMax, pinkyMax };

        //prevents null errors
        thumbCurrent = 0.0f;
        indexCurrent = 0.0f;
        middleCurrent = 0.0f;
        ringCurrent = 0.0f;
        pinkyCurrent = 0.0f;
        currentExtensions = new float [5] { thumbCurrent, indexCurrent, middleCurrent, ringCurrent, pinkyCurrent };

        //default is index clicking
        SwitchClickingFinger("thumb");//this line is the problem
        SetSelectionType("inTarget");
        allFingers = new GameObject[5] { thumb, index, middle, ring, pinky };
    }

    // Update is called once per frame
    void Update()
    {
        #region extension
        for (int i = 0; i <= 4; i++)
        {
            currentExtensions[i] = Vector3.Distance(centerPoint.position, allFingers[i].transform.position);
            if (calibration)
            {
                notCalibratedYet = false;
                if(currentExtensions[i] <= minExtensions[i])
                {
                    minExtensions[i] = currentExtensions[i];
                    AdjustMax();
                }
                if (currentExtensions[i] >= maxExtensions[i])
                {
                    maxExtensions[i] = currentExtensions[i];
                    AdjustMax();
                }
            }
        }
        if (!notCalibratedYet)//until it's been calibrated, we want the bird to stay exactly where the hand is
        {
            GetCurrentHandExtensions();//adjust, count only non-clicking fingers
            thisFrame = adjustedHandExtension * (maxDistance / adjustedMax);
            extensionDistance = (thisFrame + lastFrame) / 2.0f;
            extensionDistance -= setBack;
            lastFrame = thisFrame;
            if (!isLeft)
            {
                extensionDistance *= -1.0f;
            }
            bird.transform.localPosition = new Vector3(0.0f, extensionDistance, 0.0f);
        }
        #endregion extension

        #region clicking
        if (selectionType == "nearest" && !holding){
            GameObject closest = testManager.FindNearestGrabbable(bird.transform);
            if (closest != touchedObject)
            {
                if(touchedObject != null)
                {
                    //unhilight old
                    Unhilight(touchedObject);
                }
                touchedObject = closest;
                //hilight new
                Hilight(touchedObject);
            }
        }
        ClickFingerDisplay.text = clickingFinger.name;
        float clickAmount = Vector3.Distance(centerPoint.position, clickingFinger.transform.position);
        if (clickAmount <= clickRangeMin)//lower click amount = more bent finger
        {
            if (!clicking && antiBounceFramesTimer >= antiBounceFrames)//prevents rapid clicking and unclicking when the Quest looses tracking
            {
                antiBounceFramesTimer = 0;
                AreWeClicking.text = "true";
                if (!clicking && !holding)//select
                {
                    testManager.LogArbitrary("click! at " + Time.time + "***");
                    //first, put down anything else you might be holding
                    foreach (Transform child in bird.transform) {
                        child.parent = targetManager.transform;
                    }
                    if (touchedObject != null)
                    {
                        touchedObject.transform.parent = bird.gameObject.transform;
                        testManager.LogArbitrary("Selected  " + touchedObject.name + " which is a " + touchedObject.tag + " object at " + touchedObject.transform.position + " while the bird was at " + bird.transform.position + "***");
                    }
                    else
                    {
                        testManager.LogArbitrary("Clicked, but selected nothing while the bird was at " + bird.transform.position + "***");
                    }
                    clicking = true;
                    clickOn.Play();
                    holding = true;
                }
                if (!clicking && holding)//deselect
                {
                    clicking = true;
                    clickOff.Play();
                    Deselect();
                    testManager.LogArbitrary("Deselected  " + touchedObject.name + " which is a " + touchedObject.tag + " object at " + touchedObject.transform.position + " while the bird was at " + bird.transform.position + "***");
                }
                // writer.Close();
            }
            else
            {
                antiBounceFramesTimer++;
            }
        }
        else if(clickAmount >= clickRangeMax)
        {
            if (clicking && antiBounceFramesTimer >= antiBounceFrames)//prevents rapid clicking and unclicking when the Quest looses tracking
            {
                antiBounceFramesTimer = 0;
                clicking = false;
                AreWeClicking.text = "false";
            }
            else
            {
                antiBounceFramesTimer++;
            }
        }
        #endregion clicking
    }
    public void informOfTouch(GameObject wasTouched)
    {
        //console.log("informed of touch");runs
        if (wasTouched.tag == "grabbable" || wasTouched.tag == "red")
        {
            if(selectionType == "inTarget")
            {
                //you have no idea if specifying that wastouched can't be null will fix the problem--it doesn't
                if (touchedObject == null || wasTouched != touchedObject)//if we haven't touched anything yet, or if the thing we just touched isn't touched object
                {
                    //unhilight whatever we were touching
                    if (touchedObject != null)
                    {
                        Unhilight(touchedObject);
                    }
                    //hilight whatever we're touching now
                    Hilight(wasTouched);
                    touchedObject = wasTouched;
                }
            }
        }
    }

    public void SwitchClickingFinger(string finger)
    {
        console.log("finger selected: " + finger);
        if(finger == "index")
        {
            clickingFinger = index;
            SetClickThreshold(1);
            ClickFingerDisplay.text = clickingFinger.name;
        }
        else if(finger == "thumb")
        {
            clickingFinger = thumb;
            SetClickThreshold(0);
            ClickFingerDisplay.text = clickingFinger.name;
        }
        else
        {
            console.log("SwitchClickingFinger() supplied with invalid option.  Please switch to either index or thumb.");
        }
        AdjustMax();
    }
    public void SetSelectionType(string st)
    {
        console.log("selection type set: " + st);
        if (st == "inTarget")
        {
            selectionType = "inTarget";
        }
        else if (st == "nearest")
        {
            selectionType = "nearest";
        }
        else
        {
            console.log("SetSelectionType() supplied with invalid option.  Please switch to either inTarget or nearest.");
        }
    }

    private void Hilight(GameObject obj)
    {
        if(obj.tag == "grabbable")
        {
            obj.GetComponent<Renderer>().material = defaultHilight;
        }
        if (obj.tag == "red")
        {
            obj.GetComponent<Renderer>().material = redHilight;
        }
        if (obj.tag == "blue")
        {
            obj.GetComponent<Renderer>().material = blueHilight;
        }
    }
    private void Unhilight(GameObject obj)
    {
        if (obj.tag == "grabbable")
        {
            obj.GetComponent<Renderer>().material = defaultMat;
        }
        if (obj.tag == "red")
        {
            obj.GetComponent<Renderer>().material = red;
        }
        if (obj.tag == "blue")
        {
            obj.GetComponent<Renderer>().material = blue;
        }
    }

    private void AdjustMax()
    {
        adjustedMax = 0.0f;
        adjustedMax += (maxExtensions[2] - minExtensions[2]);//middle
        adjustedMax += (maxExtensions[3] - minExtensions[3]);//ring
        adjustedMax += (maxExtensions[4] - minExtensions[4]);//pinky
        //do index or thumb last, because they're weird.  Remember to call this every time we switch fingers.
        if (clickingFinger == index)
        {
            adjustedMax += (maxExtensions[0] - minExtensions[0]);//thumb
        }
        else if (clickingFinger == thumb)
        {
            adjustedMax += (maxExtensions[1] - minExtensions[1]);//index
        }
    }

    //call when finger is switched
    //supply with 0 for thumb clicks and 1 for index clicks
    private void SetClickThreshold(int fingerId)
    {
        float range = maxExtensions[fingerId] - minExtensions[fingerId];
        clickRangeMin = (range * 0.45f) + minExtensions[fingerId];
        clickRangeMax = (range * 0.55f) + minExtensions[fingerId];
    }

    //returns the adjusted distances for each finger currently being used to measure extension
    private void GetCurrentHandExtensions()
    {
        adjustedHandExtension = 0.0f;
        //middle, ring, pinky
        for(int i = 2; i<=4; i++)
        {
            adjustedHandExtension += Vector3.Distance(centerPoint.position, allFingers[i].transform.position);
            adjustedHandExtension -= minExtensions[i];
        }
        //do index or thumb last, because they're weird.  Remember to call this every time we switch fingers.
        if (clickingFinger == index)
        {
            adjustedHandExtension += Vector3.Distance(centerPoint.position, allFingers[0].transform.position);//thumb
            adjustedHandExtension -= minExtensions[0];
        }
        else if (clickingFinger == thumb)
        {
            adjustedHandExtension += Vector3.Distance(centerPoint.position, allFingers[1].transform.position);//index
            adjustedHandExtension -= minExtensions[1];
        }
    }

    public void LogCalibrationData()
    {
        console.log("this is when callibration data gets logged");
        string minE = "Min Extensions: ";
        foreach(float extension in minExtensions)
        {
            minE += extension + "; ";
        }
        testManager.LogArbitrary(minE + "***");
        string maxE = "Max Extensions: ";
        foreach (float extension in maxExtensions)
        {
            maxE += extension + "; ";
        }
        testManager.LogArbitrary(maxE + "***");
    }
    public void Deselect()
    {
        //so /modular/!.  Truly, unlike you.
        holding = false;
        if(touchedObject != null)
        {
            touchedObject.transform.parent = targetManager.transform;
        }
    }

    //make sure that min and max values for each finger are actually different, 
    //so that Experiment Manager doesn't let folk leave the callibration phase early
    //returns true if callibration seems fine
    public bool CheckCallibration()
    {
        for (int i = 0; i <= 4; i++)
        {
            if(minExtensions[i] + 0.1f >= maxExtensions[1])//if a finger has a range of less than 0.1
            {
                return false;
            }
        }
        return true;
    }
}
