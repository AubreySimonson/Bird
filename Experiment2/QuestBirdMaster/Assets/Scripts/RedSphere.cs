﻿/**Part of Bird User Test 2, a study intended to be conducted in the Spring of 2020, during the pandemic, to make the VRST deadline in July
 * RedSphere.cs is a script that goes on the red sphere.  It tells the TestManager if it hits something.  That's it.
 * 
 * 
 * ???--->asimonso@mit.edu
 * last modified April 2020
 * 
 **/
 
 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedSphere : MonoBehaviour
{
    public TestManager testManager;

    private void Awake()
    {
        testManager = GameObject.Find("Test Manager").GetComponent<TestManager>();
        gameObject.GetComponent<Collider>().isTrigger = true;
    }

    void OnTriggerEnter(Collider other)
    {
        testManager.RedSphereHitSomething(other);
    }
}
