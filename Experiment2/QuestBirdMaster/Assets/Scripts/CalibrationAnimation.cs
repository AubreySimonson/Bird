﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalibrationAnimation : MonoBehaviour
{
    public GameObject open;
    public GameObject closed;
    private int timer;
    public int animSpeed;//number of game frames between animation frames

    // Start is called before the first frame update
    void Start()
    {
        timer = 0;
        open.SetActive(true);
        closed.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(timer <= animSpeed)
        {
            timer++;
        }
        else
        {
            timer = 0;
            Switch();
        }
    }

    void Switch()
    {
        if (open.activeInHierarchy)
        {
            open.SetActive(false);
            closed.SetActive(true);
        }
        else
        {
            open.SetActive(true);
            closed.SetActive(false);
        }
    }
}
