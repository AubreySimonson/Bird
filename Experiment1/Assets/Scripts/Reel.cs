﻿/*This script is part of the user testing for bird.  It controls one of the many control methods tested-- The fishing reel technique from Bowman et all 1997.
 * It belongs on a gameobject representing the cursor, which is a child of the hand.

   Please contact asimonso@mit.edu for more information

   August, 2019
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace cakeslice//for highlighting
{

    public class Reel : MonoBehaviour
    {
        public AudioSource clickOn;
        public AudioSource clickOff;

        public SteamVR_ActionSet actionSetEnable;
        public SteamVR_Action_Boolean hold;
        public SteamVR_Action_Boolean extend;
        public SteamVR_Action_Boolean retract;
        private GameObject touchedObject;
        public GameObject targetManager;
        private bool holding;
        private float extensionDistance;
        public float extendSpeed;

        private void Start()
        {
            touchedObject = null;
            holding = false;
            extensionDistance = 0.0f;
        }

        // Update is called once per frame
        void Update()
        {
            //button input
            if (hold.GetStateDown(SteamVR_Input_Sources.Any))
            {
                touchedObject.transform.parent = gameObject.transform;
                holding = true;
                clickOn.Play();
            }
            if (hold.GetStateUp(SteamVR_Input_Sources.Any))
            {
                touchedObject.transform.parent = targetManager.transform;
                holding = false;
                clickOff.Play();
            }
            if (extend.GetState(SteamVR_Input_Sources.Any))
            {
                extensionDistance += extendSpeed;
                transform.localPosition = new Vector3(0.0f, 0.0f, extensionDistance);
            }
            if (retract.GetState(SteamVR_Input_Sources.Any))
            {
                extensionDistance -= extendSpeed;
                transform.localPosition = new Vector3(0.0f, 0.0f, extensionDistance);
            }
            //remove objects stuck to the hand, if they don't un-stick when they're supposed to
            if (!holding && gameObject.transform.childCount > 0)
            {
                //will only un-stick one object per frame
                gameObject.transform.GetChild(0).transform.parent = targetManager.transform;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "grabbable")
            {
                other.gameObject.AddComponent<Outline>();
                //other.gameObject.GetComponent<Outline>().color = 2;
                touchedObject = other.gameObject;
            }
        }
        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.tag == "grabbable")
            {
                Destroy(other.gameObject.GetComponent<Outline>());
                if (!holding)
                {
                    touchedObject = null;
                }
            }
        }
    }
}
