﻿/*This script is part of the user testing for bird.  It controls one of the many control methods tested-- GoGo.
 * It belongs on a gameobject representing the cursor, which is a child of the hand, with the local y axis pointed 
 * forward, away from the hand.

   Please contact asimonso@mit.edu for more information

   August, 2019
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace cakeslice//for highlighting
{

    public class GoGo : MonoBehaviour
    {
        public AudioSource clickOn;
        public AudioSource clickOff;

        public GameObject bodyCenter;
        public float closeRange;//how far does the hand need to be from the body center before we start scaling?
        public SteamVR_ActionSet actionSetEnable;
        public SteamVR_Action_Boolean hold;
        private GameObject touchedObject;
        public GameObject targetManager;
        private bool holding;

        private void Start()
        {
            touchedObject = null;
            holding = false;
        }

        // Update is called once per frame
        void Update()
        {
            //movement
            float realDistance = Vector3.Distance(bodyCenter.transform.position, transform.parent.transform.position);
            float extensionDistance = 0.0f;
            extensionDistance = Mathf.Pow(realDistance * 1.5f, realDistance + closeRange + 15.0f);
            transform.localPosition = new Vector3(0.0f, 0.0f, extensionDistance);


            //button input
            if (hold.GetStateDown(SteamVR_Input_Sources.Any))
            {
                touchedObject.transform.parent = gameObject.transform;
                holding = true;
                clickOn.Play();
            }
            if (hold.GetStateUp(SteamVR_Input_Sources.Any))
            {
                touchedObject.transform.parent = targetManager.transform;
                holding = false;
                clickOff.Play();
            }
            //remove objects stuck to the hand, if they don't un-stick when they're supposed to
            if (!holding && gameObject.transform.childCount > 0)
            {
                //will only un-stick one object per frame
                gameObject.transform.GetChild(0).transform.parent = targetManager.transform;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "grabbable")
            {
                other.gameObject.AddComponent<Outline>();
                //other.gameObject.GetComponent<Outline>().color = 2;
                touchedObject = other.gameObject;
            }
        }
        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.tag == "grabbable")
            {
                Destroy(other.gameObject.GetComponent<Outline>());
                if (!holding)
                {
                    touchedObject = null;
                }
            }
        }
    }
}
