﻿/*This script is part of the user testing for bird.  It generates 3d primitives with a color, and word on them,
   each chosen randomly from a set.  

   Please contact asimonso@mit.edu for more information

   August, 2019
 */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetGenerator : MonoBehaviour
{
    private List<Material> colors = new List<Material>();//materials also contain words
    public int numObjects;
    public float maxYDistance;
    public float maxXDistance;
    public float maxZDistance;
    public float minScale;//should be >0
    public float maxScale;//should be >0
    public GameObject targetManager;


    // Start is called before the first frame update
    private void Start()
    {
        //side rant that working with the resources folder is generally super annoying.
        Object[] dumbtempconverter = Resources.LoadAll("Materials", typeof(Material));
        Debug.Log("dumbtempoconverter.len: " + dumbtempconverter.Length);
        foreach(Object color in dumbtempconverter)//unity really does not trust that these are the right type
        {
            colors.Add((Material)color);
        }
    }
    void Update()
    {
        if (Input.GetKeyDown("g"))
        {
            //delete all old objects
            foreach (Transform child in targetManager.transform)
            {
                Destroy(child.gameObject);
            }
                //generate new objects
                for (int i = 0; i <= numObjects; i++)
            {
                GenerateObject();
            }
        }
        
    }

    private void GenerateObject()
    {
        float yPos = Random.Range(maxYDistance * -1, maxYDistance);
        float xPos = Random.Range(maxXDistance * -1, maxXDistance);
        float zPos = Random.Range(maxZDistance * -1, maxZDistance);
        int primitiveType = Random.Range(0, 3);//we can increase this number if we decide we want to use some fancy custom prefabs
        GameObject target = null;
        if(primitiveType == 0)
        {
            target = GameObject.CreatePrimitive(PrimitiveType.Cube);
        }
        if (primitiveType == 1)
        {
            target = GameObject.CreatePrimitive(PrimitiveType.Capsule);
        }
        if (primitiveType == 2)
        {
            target = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        }
        if (primitiveType == 3)
        {
            target = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        }
        Renderer rend = target.GetComponent<Renderer>();
        rend.material = colors[Random.Range(0, colors.Count)];
        target.transform.parent = gameObject.transform;
        target.transform.localPosition = new Vector3(xPos, yPos, zPos);
        target.transform.localScale *= Random.Range(minScale, maxScale);
        target.tag = "grabbable";
    }
}
