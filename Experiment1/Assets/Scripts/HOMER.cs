﻿/*This script is part of the user testing for bird.  It controls one of the many control methods tested-- HOMER.
 * It belongs on a gameobject representing the cursor, which is a child of the hand, with the local y axis pointed 
 * forward, away from the hand.

   Please contact asimonso@mit.edu for more information

   August, 2019
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace cakeslice//for highlighting
{

    public class HOMER : MonoBehaviour
    {
        public SteamVR_ActionSet actionSetEnable;
        public SteamVR_Action_Boolean hold;
        private GameObject touchedObject;
        public GameObject targetManager;
        private bool holding;
        private Ray ray;
        private int smoothingLag;//to avoid weird flickering
        public GameObject hand;
        private Vector3 collisionLocation;

        private void Start()
        {
            touchedObject = null;
            holding = false;
         
            ray = new Ray(transform.position, Vector3.forward);
            smoothingLag = 10;
        }

        // Update is called once per frame
        void Update()
        {
            if (!holding)//laser management
            {
                RaycastHit hit;
                // Does the ray intersect any objects excluding the player layer
                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
                {
                    Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                    if (hit.collider.tag == "grabbable")
                    {
                        collisionLocation = hit.point;
                        touchedObject = hit.collider.gameObject;
                        if (touchedObject.GetComponent<Outline>() == null)
                        {
                            touchedObject.gameObject.AddComponent<Outline>();
                        }
                        Debug.Log(touchedObject.name);
                        smoothingLag = 10;
                    }
                    else
                    {
                        if (smoothingLag <= 0)
                        {
                            //you're being really thorough, to prevent the issue where some would get stuck highlighted
                            Outline[] highlighted = FindObjectsOfType(typeof(Outline)) as Outline[];

                            foreach (Outline highlight in highlighted)
                            {
                                Destroy(highlight);
                            }
                            touchedObject = null;
                        }
                        else
                        {
                            smoothingLag -= 1;
                        }
                    }
                    //remove objects stuck to the hand, if they don't un-stick when they're supposed to
                    if (!holding && gameObject.transform.childCount > 0)
                    {
                        //will only un-stick one object per frame
                        gameObject.transform.GetChild(0).transform.parent = targetManager.transform;
                    }
                }
            }//end laser management
            //button input
            if (hold.GetStateDown(SteamVR_Input_Sources.Any))
            {
                if(touchedObject != null){
                    gameObject.transform.position = collisionLocation;
                    touchedObject.transform.parent = gameObject.transform;
                    holding = true;
                }
            }
            if (hold.GetStateUp(SteamVR_Input_Sources.Any))
            {
                touchedObject.transform.parent = targetManager.transform;
                holding = false;
                gameObject.transform.position = hand.transform.position;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "grabbable")
            {
                other.gameObject.AddComponent<Outline>();
                //other.gameObject.GetComponent<Outline>().color = 2;
                touchedObject = other.gameObject;
            }
        }
        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.tag == "grabbable")
            {
                Destroy(other.gameObject.GetComponent<Outline>());
                if (!holding)
                {
                    touchedObject = null;
                }
            }
        }
    }
}
