﻿/*This script is part of the user testing for bird. 
 * It automates some aspects of data collection.  Hit the space bar when the user has definitively 
 * started the test, and the spacebar again when they're done.  It will print how long they took, and 
 * the exact coords of all of the objects in the scene to the console, for easy copy-pasting to a spreadsheet
 * 
 * It should go on an empty game object in the scene called "Test Manager"
 * 
   Please contact asimonso@mit.edu for more information

   August, 2019
 */
 
 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class TestManager : MonoBehaviour
{
    private bool recording = false;
    private float timer = 0.0f;
    public GameObject targetManager;
    private string path = "Assets/Resources/data.txt";
    private GameObject cursor;
    public GameObject reel, worldScale, bird, goGo;
    private string cursorLocations = "";

    // Start is called before the first frame update
    void Start()
    {
        //find the cursor whose activity we should record
        if (reel.activeInHierarchy)
        {
            cursor = reel;
        }
        else if (worldScale.activeInHierarchy)
        {
            cursor = worldScale;
        }
        else if (bird.activeInHierarchy)
        {
            cursor = bird;
        }
        else if (goGo.activeInHierarchy)
        {
            cursor = goGo;
        }
        else {
            Debug.Log("No cursor selected!");
        }
    }

    // Update is called once per frame
    void Update()
    {

        //track cursor location
        if(cursor != null)
            cursorLocations += cursor.transform.position;
        //record to file
        if (Input.GetKeyDown("space"))
        {
            if (!recording)
            {
                recording = true;
                Debug.Log("recording!");
                //start timer
            }
            else
            {
                //Write some text to the test.txt file
                StreamWriter writer = new StreamWriter(path, true);

                recording = false;
                //print some identifying info
                writer.WriteLine("-------------------------------------------------------------------------------------------");
                writer.WriteLine("Cursor type:");
                if(cursor != null)
                    writer.WriteLine(cursor.name);
                //print timer
                writer.WriteLine("Time:");
                writer.WriteLine(timer);
                Debug.Log(timer);

                //print all obj locations
                string output = "";
                foreach (Transform child in targetManager.transform)
                {
                    output += child.position;
                    output += ";";
                }
                writer.WriteLine("Coordinates of all objects at last frame:");
                writer.WriteLine(output);

                //print cursor path
                writer.WriteLine("Cursor Locations:");
                writer.WriteLine(cursorLocations);

                writer.Close();

            }
        }

        if (recording)
        {
            timer += Time.deltaTime;
        }
    }
}
