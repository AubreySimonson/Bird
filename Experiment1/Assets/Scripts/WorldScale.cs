﻿/*This script is part of the user testing for bird.  It controls one of the many control methods tested-- the world scaling technique from Mine 1997.
 * It belongs on a gameobject representing the cursor, which is a child of the hand.
 * Remember to connect the player in the inspector.

   Please contact asimonso@mit.edu for more information

   August, 2019
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace cakeslice//for highlighting
{

    public class WorldScale : MonoBehaviour
    {
        public AudioSource clickOn;
        public AudioSource clickOff;

        public GameObject bodyCenter;
        public GameObject player;
        public float closeRange;//how far does the hand need to be from the body center before we start scaling?
        public SteamVR_ActionSet actionSetEnable;
        public SteamVR_Action_Boolean hold;
        private GameObject touchedObject;
        public GameObject targetManager;
        private bool holding;
        private Vector3 originalScale;//fix this issue where the objects that you hold scale with you.

        private void Start()
        {
            touchedObject = null;
            holding = false;
        }

        // Update is called once per frame
        void Update()
        {
            //movement
            float realDistance = Vector3.Distance(bodyCenter.transform.position, transform.parent.transform.position);
            realDistance /= player.transform.localScale.x;//prevent infinite runaway scaling
            if (realDistance >= closeRange)
            {
                float scalesize = ((realDistance - closeRange) * 25.0f) + 1.0f;//should never be smaller than 1-- we aren't ever shrinking the player
                player.transform.localScale = new Vector3(scalesize, scalesize, scalesize);
                player.transform.localPosition = new Vector3(0.0f, scalesize / -2.0f, 0.0f);//the player tends to get unusably tall
            }

            //button input
            if (hold.GetStateDown(SteamVR_Input_Sources.Any))
            {
                originalScale = touchedObject.transform.localScale;
                Debug.Log("original scale: " + originalScale);
                touchedObject.transform.parent = gameObject.transform;
                holding = true;
                clickOn.Play();
            }
            if (hold.GetStateUp(SteamVR_Input_Sources.Any))
            {
                touchedObject.transform.parent = targetManager.transform;
                //touchedObject.transform.localScale = originalScale;
                Debug.Log("original scale: " + originalScale);
                holding = false;
                clickOff.Play();
            }

            //remove objects stuck to the hand, if they don't un-stick when they're supposed to
            if(!holding && gameObject.transform.childCount > 0)
            {
                //will only un-stick one object per frame
                gameObject.transform.GetChild(0).transform.parent = targetManager.transform;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "grabbable")
            {
                other.gameObject.AddComponent<Outline>();
                touchedObject = other.gameObject;
            }
        }
        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.tag == "grabbable")
            {
                Destroy(other.gameObject.GetComponent<Outline>());
                if (!holding)
                {
                    touchedObject = null;
                }
            }
        }
    }
}
