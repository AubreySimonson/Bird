#define THUMB 4
#define INDEX 3 
#define MIDDLE 2
#define RING 1
#define PINKY 0
#define CAL_RST_PIN 8
#define SMOOTH_SAMPLES 100
#define LOOP_MS 5

int i = 0;
int j = 0;
int smooth_i = 0;
const int HAND[] = {THUMB, INDEX, MIDDLE, RING, PINKY};
const byte handLength = sizeof(HAND) / sizeof(int);

byte channel;
long bendVoltage;
unsigned long lastLoopTime = 0;
unsigned long thisLoopTime;
int bendValue;
int lowbounds[] = {1023, 1023, 1023, 1023, 1023};
int highbounds[] = {0, 0, 0, 0, 0};
int smoothBuff[SMOOTH_SAMPLES * handLength];
long smoothAvg;
bool reset;

void setup() {
  Serial.begin(115200);
}

void loop() {
  reset = digitalRead(CAL_RST_PIN);
  for (i = 0; i < handLength; i++) {
    channel = HAND[i];
    bendVoltage = analogRead(channel);
    highbounds[i] = max(highbounds[i], bendVoltage);
    lowbounds[i] = min(lowbounds[i], bendVoltage);
    // map output linearly from range (high bound, low bound] to [0, 1024)
    bendValue = 1023 - min(1023, max(0, (bendVoltage - lowbounds[i]) * 1024 / (highbounds[i] - lowbounds[i])));
    smoothBuff[i * SMOOTH_SAMPLES + smooth_i] = bendValue;
    if (reset) {
      lowbounds[i] = 1023;
      highbounds[i] = 0;
    }
  }
  smooth_i++;
  if (smooth_i >= SMOOTH_SAMPLES) {
    smooth_i = 0;
  }
  thisLoopTime = millis();
  if (thisLoopTime - lastLoopTime >= LOOP_MS) {
    lastLoopTime = thisLoopTime;
    Serial.print(" "); // mark start of frame
    for (i = 0; i < handLength; i++) {
      smoothAvg = 0;
      for (j = 0; j < SMOOTH_SAMPLES; j++) {
        smoothAvg += smoothBuff[j + i * SMOOTH_SAMPLES];
      }
      smoothAvg /= SMOOTH_SAMPLES;
      Serial.print(smoothAvg);
      Serial.print(' ');
    }
    Serial.println(';'); // \n indicates end of frame
  }
}
