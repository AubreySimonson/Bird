# Bird

All files from both Bird studies should live here

Experiment 1 Contains:

[X]Unity Project for study 1: Assets 
	-This is only the Assets folder. Import necessary files into a fresh project in Unity 2018.4.x
	-fingers.ino gets data from the glove
[X]Data from study 1


Experiment 2 Contains:

[X]Unity Project for study 2: QuestBirdMaster
	-This is only the Assets folder. Import necessary files into a fresh project in Unity 2018.4.x
[]Data from study 2

